# TechNeuronsMachineTest
This project is a Machine Test which is conducted for the position of Asp.Net Developer 
Create a new ASP.NET MVC or ASP.NET project in Visual Studio. 
Define a simple entity model Task or Employee with basic attributes like ID, Name, Description etc. 
Used Entity Framework to interact with a database 
CRUD Operations: 
Create: Allow users to add new tasks/employees. 
Read: Display a list of tasks/employees with details. 
Update: Allow users to edit existing tasks/employees. 
Delete: Provide the option to delete tasks/employees. 
Validation: Implement basic validation for input fields. 
Ensured that required fields are marked and handle validation errors gracefully. 
Designed a simple and intuitive user interface. 
Use Bootstrap or any other CSS framework for styling if desired. 
Include navigation links between the list view, create/edit views, and ensure a good user flow. 
Strictly use any version control system 
These are the Requirements given to do.And they have been sucessfully implemented and saved in the git repository 
Scripts for database entity is included in the folder 'Scripts' as MachineTestDB
